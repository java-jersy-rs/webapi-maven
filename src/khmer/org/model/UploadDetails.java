package khmer.org.model;

import javax.json.bind.annotation.JsonbProperty;

public class UploadDetails {

	@JsonbProperty("FILE_NAME")
	private String filename;

	@JsonbProperty("DATA_URL")
	private String dataUrl;

	@JsonbProperty("SIZE")
	private Long size;

	@JsonbProperty("SERVER_URL")
	private String serverUrl;

	public UploadDetails() {

	}

	public UploadDetails(String filename, String dataUrl, Long size, String serverUrl) {
		super();
		this.filename = filename;
		this.dataUrl = dataUrl;
		this.size = size;
		this.serverUrl = serverUrl;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getDataUrl() {
		return dataUrl;
	}

	public void setDataUrl(String dataUrl) {
		this.dataUrl = dataUrl;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getServerUrl() {
		return serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

}
