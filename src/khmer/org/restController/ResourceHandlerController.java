package khmer.org.restController;

import java.io.File;
import java.io.FilenameFilter;

import javax.activation.MimetypesFileTypeMap;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("")
public class ResourceHandlerController {
	// array of supported extensions
	static final String[] EXTENSIONS = new String[] { "jpg", "jpeg", "gif", "png", "bmp" };

	// filter to identify images based on their extensions
	static final FilenameFilter IMAGE_FILTER = new FilenameFilter() {

		@Override
		public boolean accept(final File dir, final String name) {
			for (final String ext : EXTENSIONS) {
				if (name.endsWith("." + ext)) {
					return (true);
				}
			}
			return (false);
		}
	};

	@GET
	@Path("/image/{image}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getImage(@PathParam("image") String image) {
		File f = new File("C:\\uploadedFiles\\" + image);
		if (!f.exists()) {
			throw new WebApplicationException(404);
		}
		String mt = new MimetypesFileTypeMap().getContentType(f);
		return Response.ok(f, mt).build();
	}

}
