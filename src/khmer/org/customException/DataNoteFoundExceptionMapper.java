package khmer.org.customException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import khmer.org.model.ErrorMessage;

public class DataNoteFoundExceptionMapper implements ExceptionMapper<DataNotFoundException> {

	@Override
	public Response toResponse(DataNotFoundException ex) {
		ErrorMessage errorMessage = new ErrorMessage(ex.getMessage(), "404", "http://localhost:8080/");
		return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
	}

}
