package khmer.org.customException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import khmer.org.model.ErrorMessage;

public class GenericExptionMapper implements ExceptionMapper<Throwable> {

	@Override
	public Response toResponse(Throwable ex) {
		ErrorMessage errorMessage = new ErrorMessage(ex.getMessage(), "500", "http://localhost:8080/");
		return Response.status(Response.Status.NOT_FOUND).entity(errorMessage).build();
	}

}
